provider "kind" {}

resource "kind_cluster" "default" {
  name = "test-cluster"
  node_image = "kindest/node:v1.16.1"
}
provider "kubectl" {
  config_context   = kind_cluster.default.name
  load_config_file = false
}

data "kubectl_file_documents" "namespace" {
  content = file("${path.module}/manifests/argocd/namespace.yaml")
}

data "kubectl_file_documents" "argocd" {
  content = file("${path.module}/manifests/argocd/install.yaml")
}

resource "kubectl_manifest" "namespace" {
  count              = length(data.kubectl_file_documents.namespace.documents)
  yaml_body          = element(data.kubectl_file_documents.namespace.documents, count.index)
  override_namespace = "argocd"
}

resource "kubectl_manifest" "argocd" {
  depends_on = [
    kubectl_manifest.namespace,
  ]
  count              = length(data.kubectl_file_documents.argocd.documents)
  yaml_body          = element(data.kubectl_file_documents.argocd.documents, count.index)
  override_namespace = "argocd"
}
